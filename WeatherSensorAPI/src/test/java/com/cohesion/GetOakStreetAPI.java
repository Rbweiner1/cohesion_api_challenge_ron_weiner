package com.cohesion;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class GetOakStreetAPI {
	
	RequestSpecification httpRequest = null;
	Response response = null;
	
	//Initialize request before calling method
	@BeforeMethod
	public void initializeRequestURL() {
		RestAssured.baseURI = "https://data.cityofchicago.org";
		httpRequest=RestAssured.given();
	}
	
	//This method gets all station_names with the name "Oak Street Weather Station"
	//No sorted order ued
	
	@Test
	public void getOakStreetRequest() {
		Response response = httpRequest.queryParam("station_name", "Oak Street Weather Station")
		              .get("/resource/k7hf-8y75.json");
		
		//Save response to a variable and print the response
		String jsonString = response.asString();
		System.out.println(jsonString);
	}
}
