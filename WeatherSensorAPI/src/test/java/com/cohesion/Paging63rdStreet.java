package com.cohesion;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Paging63rdStreet {
	RequestSpecification httpRequest = null;
	Response response = null;
	
	//Initialize request before calling methods
	@BeforeMethod
	public void initializeRequestURL() {
		RestAssured.baseURI = "https://data.cityofchicago.org";
		httpRequest=RestAssured.given();
	}
	
	/* This method retrieves the first 10 records for stations named 63rdStreetWeatherStation only for the
	 * year 2019 and limits the measurements for a page to 10.  I sorted by measurement_timestamp on my own to make sure
	 * there are no repeats when the second page is displayed.
	 */
	
	@Test (priority=1)
	public void getOakStreetRequestPageOne() {
		Response response = httpRequest.queryParam("$where","starts_with(measurement_id,'63rdStreetWeatherStation2019')")
				      .queryParam("$limit",10)
				      .queryParam("$offset",0)
				      .queryParam("$order","measurement_timestamp")
		              .get("/resource/k7hf-8y75.json");
		//Get the response, save to a variable and print the entire record
		String jsonString = response.asString();
		System.out.println(jsonString);
	}
	
	/* This method retrieves the second page of 10 records for stations named 63rdStreetWeatherStation only for the
	 * year 2019 and limits the measurements for a page to 10.  
	 */
	
	@Test (priority=2)
	public void getOakStreetRequestPageTwo() {
		Response response = httpRequest.queryParam("$where","starts_with(measurement_id,'63rdStreetWeatherStation2019')")
				      .queryParam("$limit",10)
				      .queryParam("$offset",10)
				      .queryParam("$order","measurement_timestamp")
		              .get("/resource/k7hf-8y75.json");
		
		//Get the response, save to a variable and print the entire record
		String jsonString = response.asString();
		System.out.println(jsonString);
	}
}
