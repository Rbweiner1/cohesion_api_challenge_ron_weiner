package com.cohesion;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class CheckBatteryLevel {
	RequestSpecification httpRequest = null;
	Response response = null;
	
	//Initialize request before calling method
	@BeforeMethod
	public void initializeRequestURL() {
		RestAssured.baseURI = "https://data.cityofchicago.org";
		httpRequest=RestAssured.given();
	}	
	
	@Test
	/* This particular method was created to test a negative test condition.  If the "battery_level < FULL"
	 * the test will display a message "COULD NOT PARSE SoQL QUERY" instead of failing with status code = 400.
	 * If the condition being checked is not a "malformed.compiler" type of error than the test will fail with
	 * an "UNKNOWN ERROR ENCOUNTERED" message in using a Hard Assert.
	 */
	public void batteryLife() {
		
		//Get request using an invalid query paramater for battery_level
		Response response = httpRequest.queryParam("$where","battery_life < FULL")
		              .get("/resource/k7hf-8y75.json");
		
		/*These two lines get the response, saves to variable and
		//prints the response to check where the error code is */
		//Do not need these two lines in theory
		String jsonString = response.asString();
		System.out.println(jsonString);
		
		//Save the error code response to a variable
		String code = response.jsonPath().getString("code");
	
		/*Convert to upper case (that's what the user story showed) and check to see if the code contains the text given in the story
		 It probbaly will not even be necessary to convert to upper case but I did
		*/
	    if (code.toUpperCase().contains("COMPILER.MALFORMED".toUpperCase())) {
	    	System.out.println("\t\tCOULD NOT PARSE SoQL QUERY\n");
	    }
	    else {
	    	Assert.fail("UNKNOWN ERROR ENCOUNTERED");
	    }
	    
        }
	    	
}

